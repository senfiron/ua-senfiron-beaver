﻿Shader "Custom/Mask"
{
        Properties
        {
                _MainTex ("Base (RGB)", 2D) = "white" {}
                _MaskTex ("Mask (A)", 2D) = "white" {}
        }
        SubShader
        {
        Tags { "Queue" = "Transparent" }
        Lighting On
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        
        Pass
                {
            //Blend SrcAlpha OneMinusSrcAlpha
            SetTexture [_MaskTex] { combine texture }
            SetTexture [_MainTex] { combine texture, previous } 
        }
    }
}