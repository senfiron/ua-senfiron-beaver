﻿using UnityEngine;
using System.Collections;
using System.Linq;
    
public class GameController : MonoBehaviour
{
    public GameDifficulty[] difficulties;
    public Beaver beaverPrefab;
    public Transform onHitEffectPrefab;
    public Transform onMissEffectPrefab;
    public int hearts = 3;

    private int _score = 0;
    private GameObject[] burrows;

    public int Score
    { 
        get
        {
            return _score;
        }
        private set
        {
            _score = value;
            if (_score > HighScore)
            {
                HighScore = value;
                PlayerPrefs.SetInt("HighScore", HighScore);
            }
        }
    }

    public int HighScore { get; private set; }

    public GameDifficulty Difficulty
    {
        get
        {
            if (GameSettings.difficultyIndex >= 0 && GameSettings.difficultyIndex < difficulties.Length)
            {
                return difficulties [GameSettings.difficultyIndex];
            }
            return null;
        }
    }

    void Awake()
    {
        GameSettings.isGameStarted = false;
        GameSettings.isGameOver = false;
        GameSettings.isPaused = false;
        HighScore = PlayerPrefs.GetInt("HighScore", 0);
        Input.simulateMouseWithTouches = false;
        burrows = GameObject.FindGameObjectsWithTag("Burrow");
    }

    void Start()
    {
        onMissEffectPrefab.CreatePool();
        onHitEffectPrefab.CreatePool();
        beaverPrefab.CreatePool();
    }

    void Update()
    {
        //wait while difficulty selected
        if (Difficulty != null)
        if (!GameSettings.isGameStarted)
        {
            StartGame();
        } else
        {
            if (!GameSettings.isPaused)
            if (GUIUtility.hotControl == 0)
            if (Input.GetMouseButtonDown(0))
            {
                CheckForHit(Input.mousePosition);
            } else
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        CheckForHit(touch.position);
                    }
                }
            }
        }
    }

    void StartGame()
    {
        if (Difficulty.beaverSpawnInterval > 0)
            StartCoroutine(StartBeaverSpawner());
        
        if (Difficulty.spawnSpeedIncreaseInterval > 0)
            StartCoroutine(StartSpeedIncreaser());
        
        if (Difficulty.spawnCountIncreaseInterval > 0)
            StartCoroutine(StartCountIncreaser());
        
        GameSettings.isGameStarted = true;
    }
    
    IEnumerator StartBeaverSpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(Difficulty.beaverSpawnInterval);
            SpawnBeavers();
        }
    }

    IEnumerator StartSpeedIncreaser()
    {
        while (true)
        {
            yield return  new WaitForSeconds(Difficulty.spawnSpeedIncreaseInterval);
            Difficulty.IncreaseSpawnSpeed();
        }
    }
    
    IEnumerator StartCountIncreaser()
    {
        while (true)
        {
            yield return  new WaitForSeconds(Difficulty.spawnCountIncreaseInterval);
            Difficulty.IncreaseSpawnCount();
        }
    }
    
    void SpawnBeavers()
    {
        for (int i = 0; i < Difficulty.beaverSpawnCount; i++)
        {                
            //Select spawnpoints without Beaver
            var spawns = from x in burrows where x.GetComponentsInChildren<Beaver>().Count() == 0 select x;
            
            if (spawns.Count() > 0)
            {
                var spawn = spawns.ElementAt(Random.Range(0, spawns.Count())).GetComponent<Transform>();
                
                
                bool isMadBeaver = (Difficulty.beaverMadChance >= Random.Range(0f, 1f));
                var beaver = beaverPrefab.Spawn();
                beaver.isMad = isMadBeaver;

                //parent name determines the sorting layer of child beaver
                beaver.GetComponent<SpriteRenderer>().sortingLayerName = spawn.parent.gameObject.name;
                
                beaver.transform.parent = spawn;
            }
        }
    }

    void CheckForHit(Vector3 position)
    {            
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(position), Vector2.zero);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.CompareTag("Beaver"))
            {
                onHitEffectPrefab.Spawn(hit.point);
                var beaver = hit.collider.GetComponent<Beaver>();
                beaver.Kill();
            } else
            {
                onMissEffectPrefab.Spawn(hit.point);
            }
        }
    }
    
    public void RightAction()
    {
        Score += Difficulty.GetRightActionScore();
    }
    
    public void WrongAction()
    {
        Difficulty.ResetStreakMultiplier();
        hearts--;
        
        if (hearts == 0)
        {
            StopAllCoroutines();
            GameSettings.isGameOver = true;
        }
    }

}
