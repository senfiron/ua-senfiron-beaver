﻿using UnityEngine;
using System.Collections;

public static class GameSettings
{

    public static float pausedVolumeMultiplier = 0.3f;
    public static int difficultyIndex = -1;
    public static bool isGameStarted = false;
    public static bool isGameOver = false;

    private static bool _isMuted = false;
    private static bool _isPaused = false;

    public static bool isMuted
    {
        get { return _isMuted;}
        set
        { 
            if (_isMuted != value)
            {
                _isMuted = value;
                AudioListener.pause = _isMuted;
                AudioListener.volume = _isMuted ? 0f : 1f;
            }
        }
    }

    public static bool isPaused
    {
        get{ return _isPaused;}
        set
        {

            if (_isPaused != value)
            {
                _isPaused = value;
                if (_isPaused == true)
                {
                    Time.timeScale = 0f;
                    if (!_isMuted)
                        AudioListener.volume *= pausedVolumeMultiplier;
                } else
                {
                    Time.timeScale = 1f;
                    if (!_isMuted)
                        AudioListener.volume = 1f;
                }
            }
        }
    }
}
