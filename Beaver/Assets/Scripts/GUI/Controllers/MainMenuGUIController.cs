﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuGUIController : MonoBehaviour
{

    public Toggle muteToggle;
    void Start()
    {
        GameSettings.isPaused = false;
        GameSettings.difficultyIndex = -1;
        if (muteToggle)
            muteToggle.isOn = !GameSettings.isMuted;
    }

    public void StartGame()
    {
        Application.LoadLevel("Game");
    }

    public void OpenFacebook()
    {
        Application.OpenURL("http://facebook.com");
    }

    public void OpenTwitter()
    {
        Application.OpenURL("http://twitter.com");
    }
    
    public void ChangeMute(bool isOn)
    {
        GameSettings.isMuted = !isOn;
    }
}
