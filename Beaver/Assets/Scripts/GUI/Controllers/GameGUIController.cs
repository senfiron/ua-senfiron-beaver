using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class GameGUIController : MonoBehaviour
{
    public Animator[] hearts;
    public ScoreLabel scoreLabel;
    public ScoreLabel highScoreLabel;

    public GameObject pauseWindow;
    public GameObject gameOverWindow;
    public GameObject difficultyWindow;

    private int heartsCount;
    private GameController game;

    void Awake()
    {
        game = GameObject.FindObjectOfType<GameController>();
        game.hearts = heartsCount = hearts.Length;
        if (GameSettings.difficultyIndex != -1)
            difficultyWindow.gameObject.SetActive(false);
    }

    void Update()
    {
        while (heartsCount > 0 && heartsCount > game.hearts)
        {
            hearts [--heartsCount].SetTrigger("Destroy");
        }
        scoreLabel.Value = game.Score;
        highScoreLabel.Value = game.HighScore;
        
        if (GameSettings.isGameOver)
        if (heartsCount == 0) //wait until last heart dissappears
        {
            gameOverWindow.SetActive(true);
        }
        
        if (Input.GetKey(KeyCode.Escape))
        {
            OnApplicationPause(true);
        }
    }

    public void OnApplicationPause(bool pauseStatus)
    {
        if (GameSettings.difficultyIndex >= 0)
        if (!GameSettings.isGameOver)
        if (pauseStatus == true)
        {
            pauseWindow.SetActive(true);
        }
    }

    public void ChooseDifficulty(int difficultyIndex)
    {
        GameSettings.difficultyIndex = difficultyIndex;
        difficultyWindow.SetActive(false);
    }

    public void Continue()
    {
        pauseWindow.SetActive(false);
    }

    public void PlayAgain()
    {
        Application.LoadLevel("Game");
    }
    
    public void MainMenu()
    {
        Application.LoadLevel("MainMenu");
    }

}
