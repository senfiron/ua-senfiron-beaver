﻿using UnityEngine;
using System.Collections;

public class EdgeBlackener : MonoBehaviour
{
    //black texture to hide the edges of the screen from effects
    //    when aspect ration is not optimal
    private Texture2D _blackPixel;
    private Texture2D BlackPixel
    {
        get
        {
            if (_blackPixel == null)
            {
                _blackPixel = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                _blackPixel.SetPixel(0, 0, Color.black);
                _blackPixel.filterMode = FilterMode.Point;        
                _blackPixel.Apply();
            }
            return _blackPixel;
        }
    }
    
    void OnGUI()
    {
        BlackenTheEdges(ScaleUtils.GUIRect);
    }
    
    void BlackenTheEdges(Rect GUIRect)
    {
        if (ScaleUtils.CurrentAspectRatio != ScaleUtils.OriginalAspectRatio)
        {
            Rect first, second;
            if (ScaleUtils.CurrentAspectRatio < ScaleUtils.OriginalAspectRatio)
            {
                //screen is higher
                first = new Rect()
                {
                    width = Screen.width,
                    height = (Screen.height - GUIRect.height) / 2
                };
                
                second = new Rect(first)
                {
                    y = Screen.height - first.height
                };
            } else
            {
                //screen is wider
                first = new Rect()
                {
                    width = (Screen.width - GUIRect.width) / 2,
                    height = Screen.height
                };
                
                second = new Rect(first)
                {
                    x = Screen.width - first.width
                };
            }
            
            GUI.DrawTexture(first, BlackPixel, ScaleMode.StretchToFill);
            GUI.DrawTexture(second, BlackPixel, ScaleMode.StretchToFill);
        }
    }
    

}
