﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class EventAction : MonoBehaviour
{

    [SerializeField]
    private UnityEvent
        _action = new UnityEvent();

    void StartEvent()
    {
        _action.Invoke();
    }
}
