﻿using UnityEngine;
using System.Collections;

public class PausedWindow : MonoBehaviour
{

    void OnEnable()
    {
        GameSettings.isPaused = true;
    }
    
    void OnDisable()
    {
        GameSettings.isPaused = false;
    }

}
