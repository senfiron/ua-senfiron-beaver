﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageToggle : MonoBehaviour
{

    public Image target;
    public Sprite onSprite;
    public Sprite offSprite;

    public void ToggleImage(bool state)
    {
        target.sprite = state ? onSprite : offSprite;
    }
}
