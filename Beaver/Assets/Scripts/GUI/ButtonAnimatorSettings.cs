﻿using UnityEngine;
using System.Collections;

public class ButtonAnimatorSettings : MonoBehaviour
{

    protected Animator animator;
    public float strength = 20f;
    public bool fictive = false;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        if (animator)
        {
            animator.SetBool("Fictive", fictive);
            animator.SetFloat("Strength", strength);
        }  
    }      
}
