﻿using UnityEngine;
using System.Collections;

public static class ScaleUtils
{
    public const float originalWidth = 960;
    public const float originalHeight = 640;
    public const float OriginalAspectRatio = originalWidth / originalHeight;

    public static float CurrentWidth
    {
        get
        {
            UpdateScale();
            return _currentWidth;
        }
    }

    public static float CurrentHeight
    {
        get
        {
            UpdateScale();
            return _currentHeigth;
        }
    }

    public static float CurrentAspectRatio
    {
        get
        {
            UpdateScale();
            return _currentAspectRatio;
        }
    }

    public static float ScreenScaleX
    {
        get
        {
            UpdateScale();
            return _screenScaleY;
        }
    }

    public static float ScreenScaleY
    {
        get
        {
            UpdateScale();
            return _screenScaleY;
        }
    }

    public static float ScreenScale
    {
        get
        {
            UpdateScale();
            return _screenScale;
        }
    }

    public static Rect GUIRect
    {
        get
        {
            UpdateScale();
            return _guiRect;
        }
    }

    private static float _currentWidth;
    private static float _currentHeigth;
    private static float _currentAspectRatio;
    private static float _screenScaleX;
    private static float _screenScaleY;
    private static float _screenScale;
    private static Rect _guiRect;

    private static void UpdateScale()
    {
        if (_currentWidth != Screen.width || _currentHeigth != Screen.height)
        {
            _currentWidth = Screen.width;
            _currentHeigth = Screen.height;
            _currentAspectRatio = _currentWidth / _currentHeigth;

            _screenScaleX = _currentWidth / originalWidth;
            _screenScaleY = _currentHeigth / originalHeight;
            _screenScale = (OriginalAspectRatio > _currentAspectRatio) ? _screenScaleX : _screenScaleY;

            _guiRect = new Rect();
            _guiRect.width = originalWidth * _screenScale;
            _guiRect.height = originalHeight * _screenScale;
            _guiRect.x = (_currentWidth - _guiRect.width) / 2;
            _guiRect.y = (_currentHeigth - _guiRect.height) / 2;
        }
    }

    //Rect extension methods
    public static Rect Scale(this Rect rect)
    {
        return Scale(rect, ScreenScale);
    }

    public static Rect Scale(this Rect rect, float scale)
    {
        rect.width *= scale;
        rect.height *= scale;
        rect.x *= scale;
        rect.y *= scale;
        return rect;
    }
}
