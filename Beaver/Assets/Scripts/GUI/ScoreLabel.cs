﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreLabel : MonoBehaviour
{

    public float lerpDuration = 0.25f;

    public int Value
    {
        get { return targetValue; }
        set
        {
            if (targetValue != value)
            {
                oldValue = displayValue;
                targetValue = value;
                animationStartTime = Time.realtimeSinceStartup;
            }
        }
    }

    private int targetValue;
    private int oldValue;
    private int displayValue;
    private float animationStartTime = 0f;

    void Update()
    {
        float elapsed = Time.realtimeSinceStartup - animationStartTime;
        float progress = elapsed / lerpDuration;

        if (progress >= 1f)
        {
            oldValue = displayValue = Value;
        } else
        {
            displayValue = Mathf.RoundToInt(Mathf.Lerp(oldValue, Value, progress));
        }
        GetComponent<Text>().text = string.Format("{0:D6}", displayValue);
    }
}
