﻿using UnityEngine;
using System.Collections;

public class CameraResize : MonoBehaviour
{
    private float originalSize = 10f;
    private float currentScale = 1f;

    void Start()
    {
        originalSize = Camera.main.orthographicSize;
    }
	
    void Update()
    {
        // when screen is higher than the original, we must scale up the camera accordingly
        float cameraScale = (ScaleUtils.OriginalAspectRatio / ScaleUtils.CurrentAspectRatio);

        if (currentScale != cameraScale)
        {
            if (cameraScale > 1f)
            {
                Camera.main.orthographicSize = originalSize * cameraScale;
            } else if (Camera.main.orthographicSize != originalSize)
            {
                Camera.main.orthographicSize = originalSize;
            }
            currentScale = cameraScale;
        }
    }
}
