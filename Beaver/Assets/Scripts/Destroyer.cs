﻿using UnityEngine;
using System.Collections;

//Used to destroy object from animation event
public class Destroyer : MonoBehaviour
{
    IEnumerator DestroySelf()
    {
        yield return 0;
        this.transform.Recycle();
    }
}
