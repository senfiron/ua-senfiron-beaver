using UnityEngine;
using System.Collections;

delegate void Action();

public class Beaver : MonoBehaviour
{
    public bool isMad
    {
        get { return _isMad; }
        set
        {
            _isMad = value;
            if (_isMad)
            {
                spriteRenderer.sprite = madBeaverSprite;
                killedAction = game.WrongAction;
                hiddenAction = game.RightAction;
            } else
            {
                spriteRenderer.sprite = normalBeaverSprite;
                killedAction = game.RightAction;
                hiddenAction = game.WrongAction;
            }
        }
    }

    public Sprite normalBeaverSprite;
    public Sprite madBeaverSprite;

    private Action killedAction;
    private Action hiddenAction;

    private GameController game;
    private SpriteRenderer spriteRenderer;
    private bool isKilled = false;

    private bool _isMad;

    void Awake()
    {
        game = GameObject.FindObjectOfType<GameController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        isMad = false;
    }

    void OnEnable()
    {
        animation.Play("Show");
        isKilled = false;
        StartCoroutine(HideAfterDelay(game.Difficulty.beaverHideDelay));
    }

    IEnumerator HideAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (!isKilled)
            animation.Play("HideAndDestroy");
    }

    public void Kill()
    {
        if (isKilled)
            return;
        StopAllCoroutines();
        isKilled = true;
        animation.Play("HideAndDestroy");

        killedAction();
    }

    public void TotallyHidden()
    {
        if (!isKilled)
        {
            hiddenAction();
        }
    }

    IEnumerator DestroySelf()
    {
        yield return 0;
        this.Recycle();
    }
}
