﻿using UnityEngine;
using System.Collections;

public class RandomAudioClip : MonoBehaviour
{

    public AudioClip[] audioClips;

    void OnEnable()
    {
        int index = Random.Range(0, audioClips.Length);
        audio.clip = audioClips [index];
        audio.Play();
    }
}
