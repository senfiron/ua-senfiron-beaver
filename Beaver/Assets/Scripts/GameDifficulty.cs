﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameDifficulty
{
    public string name;
    public int beaverSpawnCount = 1;
    public float beaverSpawnInterval = 2f;
    public float beaverHideDelay = 2f;
    public float beaverMadChance = 0f;

    public float spawnSpeedIncreaseInterval = 60f;
    public float spawnIntervalDivider = 1.25f;
    public float spawnCountIncreaseInterval = 0f;
    public int spawnCountIncreaseValue = 0;

    public int scorePerKill = 100;
    public float streakScoreMultiplier = 1f;
    public float multiplierPerKill = 0.5f;

    private float startingStreakScoreMultiplier = 1f;

    public GameDifficulty()
    {
        startingStreakScoreMultiplier = streakScoreMultiplier;
    }
    public void IncreaseSpawnSpeed()
    {
        if (spawnIntervalDivider != 0f)
        {
            beaverSpawnInterval /= spawnIntervalDivider;
            beaverHideDelay /= spawnIntervalDivider;
        }
    }
    public void IncreaseSpawnCount()
    {
        beaverSpawnCount += spawnCountIncreaseValue;
    }

    public int GetRightActionScore()
    {
        int score = Mathf.RoundToInt(scorePerKill * streakScoreMultiplier);
        streakScoreMultiplier += multiplierPerKill;

        return score;
    }

    public void ResetStreakMultiplier()
    {
        streakScoreMultiplier = startingStreakScoreMultiplier;
    }
}
